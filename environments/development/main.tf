variable "do_token" {
}

provider "digitalocean" {
  token = var.do_token
}

module "droplet_mysql" {
  source = "../../modules/do_droplet"

  name     = "sfo2-mysql-dev"
  tag_name = "mysql"
  tag_env  = "development"
}

module "development_project" {
  source      = "../../modules/do_project"

  name        = "Development"
  description = "development vms"
  purpose     = "development testing"
  environment = "development"
  resources   = ["${module.droplet_mysql.droplet_urn}"]
}