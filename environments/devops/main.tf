variable "do_token" {
}

provider "digitalocean" {
  token = var.do_token
}

module "droplet_nagios" {
  source = "../../modules/do_droplet"

  name     = "sfo2-nagios"
  tag_name = "nagios"
  tag_env  = "testing"
}

module "testing_project" {
  source      = "../../modules/do_project"

  name        = "CI/CD"
  description = "CI/CD automation tools"
  purpose     = "Collection of CI/CD tools"
  environment = "development"
  resources   = ["${module.droplet_jenkins.droplet_urn}"]
}