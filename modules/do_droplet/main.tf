resource "digitalocean_tag" "name" {
    name = "${var.tag_name}"
}

resource "digitalocean_tag" "environment" {
    name = "${var.tag_env}"
}

resource "digitalocean_droplet" "this" {
    name   = "${var.name}"
    image  = "${var.image}"
    region = "${var.region}"
    size   = "${var.size}"

    tags = ["${digitalocean_tag.name.id}", "${digitalocean_tag.environment.id}"]
}