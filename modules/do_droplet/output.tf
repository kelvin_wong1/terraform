output "droplet_urn" {
    description = "URN of created droplet"
    value = "${digitalocean_droplet.this.urn}"
}