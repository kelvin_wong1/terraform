variable "image" {
    description = "Image for the droplet"
    default = "centos-7-x64"
}

variable "name" {
    description = "Name for this droplet"
}

variable "region" {
    description = "Region for the droplet"
    default = "sfo2"
}

variable "size" {
    description = "Size of the droplet"
    default = "s-1vcpu-1gb"
}

variable "tag_name" {
    description = "Tag of value Name for the droplet"
}

variable "tag_env" {
    description = "Tag of value Environment for the droplet"
}