variable "name" {
    description = "Name of the project"
}

variable "description" {
    description = "Description of the project"
}

variable "purpose" {
    description = "Purpose of the project"
}

variable "environment" {
    description = "Environment of the project"
}

variable "resources" {
    description = "List of resources for this project"
    default = []
}